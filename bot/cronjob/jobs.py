import schedule
import time, threading
from config.utils import gen_schedule_moovie, md5
from db.base import check_moovie
from core.scrap.browser import Browser
from config import const
from db import base as db


def moovie_done(moovie_hash):
	print("Done")
	moovie = const.CURRENT_MOOVIES[moovie_hash]
	moovie_info = moovie["moovie_info"]

	# Public to tg
	const.send_post(moovie_info)

	# Remove scedule
	schedule.cancel_job(const.CURRENT_MOOVIES[moovie_hash]["obj"]) # Cancel schedule
	del const.CURRENT_MOOVIES[moovie_hash] # Remove the moovie from ram

	# Add moovie to db
	db.add_moovie(moovie_info["base_url"])

	# Generate new moovie
	if moovie["day"] in db.get_timetable()["days"]: # no change
		const.engine([moovie["day"]], db.get_timetable()["at"])
	else:
		print("No more day in timetable")



"""
ogni volta che aggiungi un job, metti anche su CURRENT_MOOVIES il moovie
"""

def get_sched(day, ora, job, moovie_hash):

	if day == "Monday":
		return schedule.every().monday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")
	elif day == "Tuesday":
		return schedule.every().tuesday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")
	elif day == "Wednesday":
		return schedule.every().wednesday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")

	elif day == "Thursday":
		return schedule.every().thursday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")

	elif day == "Friday":
		return schedule.every().friday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")

	elif day == "Saturday":
		return schedule.every().saturday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")

	elif day == "Sunday":
		return schedule.every().sunday.at(ora).do(job, moovie_hash=moovie_hash).tag(f"{day}:{ora}")
	else:
		return -1

def keep_session():
	const.driver.get("https://altadefinizione01-nuovo.link/")

def add_moovie_to_schedule(day, ora, moovie_hash, moovie_info):
	obj = get_sched(day, ora, moovie_done, moovie_hash)
	res = gen_schedule_moovie(moovie_hash, moovie_info, day, ora, obj)

	return res

def loop():
	schedule.every(20).minutes.do(keep_session)
	while 1:
		#print(schedule.jobs)
		schedule.run_pending()
		time.sleep(1)
	

t = threading.Thread(target=loop)
t.daemon = True
t.start()

#non serve
"""def cancel_job(old_day, old_ora, new_day=None, new_ora=None):

	schedule.clear(f"{old_day}:{ora}") # clear

	if new_day and old_ora:
		add_job(new_day, old_ora)
	elif new_ora and old_day:
		add_job(old_day, new_ora)"""

#add_job("sunday", "14:22", print_schedule)

"""while 1:
	print(dir(jobs[0].shou))
	schedule.run_pending()
	time.sleep(1)"""


"""
i have to generate a list as the thing run, 

this should be also in schedule list BOT, -> if he click: then check below
if no film in db and there is settings timeschedule, 
	add films
otherwise 
	nothing!

he create the films for the current week. OK.
then if he update the schedule time will be updated! for next week.

"""
