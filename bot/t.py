"""
This bot is created for the demonstration of a usage of inline keyboards.
"""

import logging

from aiogram import Bot, Dispatcher, executor, types

import asyncio, requests

#from db.base import add_moovie, check_moovie
from config.utils import get_day, get_hour, update_dic
from config.processor import moovies_scheduler_engine, gen_single
from config.const import (days_wrapper, hours_wrapper)

from core.scrap.browser import Browser

from db import base as db
from config import const


API_TOKEN = const.API_TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)
loop = asyncio.get_event_loop()

def send_post(moovies_info):
	br = Browser()
	image_link = br.CURRENT_URL+moovies_info["img"][1:]

	msg = ""

	attr_msg = ["quality", "film_length", "year", "genre", "name", "rating"]

	for k,v in moovies_info.items():
		if k in attr_msg:
			if v:
				if "sym" not in v:
					msg+=v+"\n"
				else:
					msg+=v["sym"] + v["value"]+"\n"
	msg+="\n"+"📁 @PupazzoFilm"

	link_adfoc = requests.get(f"http://adfoc.us/api/?key={const.token_adfocus}&url={moovies_info['own_url']}").text
	#link_bitly = const.bitly_conn.shorten(uri=link_adfoc)["url"]

	keyboard_markup = types.InlineKeyboardMarkup(row_width=1)
	row_btns = [types.InlineKeyboardButton("🎥Film", url=link_adfoc)]
	keyboard_markup.row(*row_btns)




	loop.create_task(bot.send_photo(chat_id=const.CHANNEL, photo=image_link, reply_markup=keyboard_markup, caption=msg, parse_mode="HTML"))
	#loop.create_task(bot.send_message(chat_id=const.CHANNEL, text=moovies_info))



@dp.message_handler(commands='start')
async def home(message: types.Message):
    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    usid = message.from_user.id
    
    # default row_width is 3, so here we can omit it actually
    # kept for clearness

    text_and_data = (
        ('Change Frequency', 'frequency'),
        ('Schedule List', 'schedule'),
    )
    # in real life for the callback_data the callback data factory should be used
    # here the raw string is used for the simplicity
    row_btns = (types.InlineKeyboardButton(text, callback_data=data) for text, data in text_and_data)

    keyboard_markup.row(*row_btns)


    if isinstance(message, types.Message):
        await bot.send_message(usid, "Ciao capo! che vojamo fa?", reply_markup=keyboard_markup)
    elif isinstance(message, types.CallbackQuery):
        msg_id = message.message.message_id
        await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Ciao capo! che vojamo fa?")

    


# Use multiple registrators. Handler will execute when one of the filters is OK
@dp.callback_query_handler(text='frequency')  # if cb.data == 'no'
async def frequency(query: types.CallbackQuery):
    usid = query.from_user.id
    msg_id = query.message.message_id

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)

    text_and_data = (
        ('Days', 'days'),
        ('Hours', 'hours'),
    )
    # in real life for the callback_data the callback data factory should be used
    # here the raw string is used for the simplicity
    row_btns = (types.InlineKeyboardButton(text, callback_data=data) for text, data in text_and_data)
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-home"))
    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Seleziona la frequenza:")

    await query.answer(f'You answered with ')


@dp.callback_query_handler(text='days')  # if cb.data == 'no'
async def days(query: types.CallbackQuery):
    usid = query.from_user.id
    msg_id = query.message.message_id

    days = update_dic(db.get_timetable(), "days")

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)

    row_btns = (types.InlineKeyboardButton(text, callback_data=data) for data, text in days.items())
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-frequency"))

    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Qui puoi scegliere i giorni in cui postare")

    await query.answer('')

@dp.callback_query_handler(text='hours')  # if cb.data == 'no'
async def hours(query: types.CallbackQuery):
    usid = query.from_user.id
    msg_id = query.message.message_id

    hours = update_dic(db.get_timetable(), "hours")

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)

    row_btns = (types.InlineKeyboardButton(text, callback_data=data) for data, text in hours.items())
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-frequency"))

    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Qui puoi scegliere le ore in cui postare")

    await query.answer('')

@dp.callback_query_handler(text=["h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8"]) 
async def print_hours(query: types.CallbackQuery):
    answer_data = query.data
    clear_answer_data = get_hour(answer_data)
    usid = query.from_user.id
    msg_id = query.message.message_id

    db.update_hour(clear_answer_data)

    query_answer = "Added!"

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    

    row_btns = []
    for data, text in hours_wrapper.items():
        if text == clear_answer_data:
            text += "*"
        row_btns.append(types.InlineKeyboardButton(text, callback_data=data))
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-frequency"))


    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Qui puoi scegliere le ore in cui postare")

    await query.answer(query_answer)

@dp.callback_query_handler(text=["d1", "d2", "d3", "d4", "d5", "d6", "d7"]) 
async def print_days(query: types.CallbackQuery):
    answer_data = query.data
    usid = query.from_user.id
    msg_id = query.message.message_id

    timetable = db.get_timetable()

    days = update_dic(timetable, "days")

    query_answer = "Added!"


    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    selection = {}

    for data, text in days.items():

        if data == answer_data:
            if "*" in text: # Means he want to cancel it
                db.remove_from_timetable(day=get_day(answer_data))
                text = text.replace("*", "")
                query_answer = "Removed!"
            else:
                day = get_day(answer_data)
                db.add_timetable(day=day) # Add to db
                text = text + "*"
                moovie_days = [moovie_tt["day"] for hash_moovie, moovie_tt in const.CURRENT_MOOVIES.items()]
                if day not in moovie_days:
                    moovies_scheduler_engine([day], timetable["at"])


        selection.update({data:text})


    row_btns = (types.InlineKeyboardButton(text, callback_data=data) for data, text in selection.items())
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-frequency"))

    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text="Qui puoi scegliere i giorni in cui postare")

    await query.answer(query_answer)

@dp.callback_query_handler(text='schedule') 
async def schedule(query: types.CallbackQuery):
    usid = query.from_user.id
    msg_id = query.message.message_id

    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    moovies_info = ""
    row_btns = []

    hash_day = {}

    for moovie_hash, m in const.CURRENT_MOOVIES.items():
        hash_day.update({m["day"]:moovie_hash})


    for day in sorted(hash_day.keys(), key=const.filterdays.index):
        moovie_hash = hash_day[day]
        moovie = const.CURRENT_MOOVIES[moovie_hash]
        
        moovies_info+= f"{moovie['moovie_info']['name']['value']}<b>{moovie['day']}:{moovie['at']}</b>\n\n"
        row_btns.append(types.InlineKeyboardButton(moovie["moovie_info"]["name"]["value"], callback_data=moovie_hash))
        row_btns.append(types.InlineKeyboardButton("Remove", callback_data="remove-"+moovie_hash))
        row_btns.append(types.InlineKeyboardButton("Change", callback_data="change-"+moovie_hash))
    keyboard_markup.add(*row_btns)
    keyboard_markup.add(types.InlineKeyboardButton("Indietro", callback_data="return-home"))

    await bot.edit_message_text(chat_id=usid, message_id=msg_id, reply_markup=keyboard_markup, text=moovies_info, parse_mode="HTML")

    await query.answer('')


@dp.callback_query_handler(text_contains="remove")  # if cb.data == 'no'
async def rem(query: types.CallbackQuery):
    moovie_hash = query.data.split("-")[1]
    del const.CURRENT_MOOVIES[moovie_hash]
    await schedule(query)


@dp.callback_query_handler(text_contains="change")  # if cb.data == 'no'
async def change(query: types.CallbackQuery):
    moovie_hash = query.data.split("-")[1]
    day = const.CURRENT_MOOVIES[moovie_hash]["day"]
    at = const.CURRENT_MOOVIES[moovie_hash]["at"]
    del const.CURRENT_MOOVIES[moovie_hash]
    const.NOT_TODAY.append(moovie_hash)
    gen_single(day, at)

    await schedule(query)

@dp.callback_query_handler(text_contains="return")  # if cb.data == 'no'
async def back(query: types.CallbackQuery):
    answer_data = query.data
    usid = query.from_user.id
    msg_id = query.message.message_id

    action = query.data.split("-")[1]

    if action == "frequency":
        await frequency(query)
    elif action == "home":
        await home(query)

    await query.answer('')
