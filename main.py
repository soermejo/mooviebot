import time
from threading import Thread
from config import const

from bot.t import executor, dp, send_post
from config.utils import md5
from config.processor import moovies_scheduler_engine

from core.web.server import app

const.init()
const.send_post = send_post
const.engine = moovies_scheduler_engine


# Proc 1
def start_bot():
	executor.start_polling(dp, skip_updates=True) 
def run_server():
	app.run(host='0.0.0.0')




#moovies_scheduler_engine()
p1 = Thread(target=start_bot)
p2 = Thread(target=run_server)
p3 = Thread(target=const.engine)

p1.daemon = True
p2.daemon = True
p3.daemon = True

p1.start()
p2.start()
p3.start()


while 1:
	time.sleep(2)
	pass