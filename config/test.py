
#!/usr/local/bin/python
"""
This is a py.test script
Example usage on Unix:
bitly-api-python $ BITLY_ACCESS_TOKEN=<accesstoken> nosetests
or 'export' the two environment variables prior to running nosetests
"""
import os
import sys
sys.path.append('../')
import bitly_api

API_USER = "o_5li5bvfc84"
API_KEY = "bc2effc32d0e85174c887e287db065cfb346d9ba"



def get_connection():
    """Create a Connection base on username and access token credentials"""
    if BITLY_ACCESS_TOKEN not in os.environ:
        raise ValueError("Environment variable '{}' required".format(BITLY_ACCESS_TOKEN))
    access_token = os.getenv(BITLY_ACCESS_TOKEN)
    bitly = bitly_api.Connection(access_token=access_token)
    return bitly

bitly = get_connection()
data = bitly.shorten('http://google.com/')
print(data)