from config import const

from db.base import get_timetable, add_moovie, check_moovie, remove_from_timetable, add_timetable
from bot.cronjob.jobs import add_moovie_to_schedule
from config.utils import md5
from core.scrap.browser import Browser

def moovies_scheduler_engine(days=None, at=None):

	if days and at:
		res = {"days":days, "at":at}
	else:
		res = get_timetable()

	if not res["days"] or not res["at"]:
		pass
	else:
		br = Browser()
		page = 0
		already_choosen = []

		for day in res["days"]:
			while 1:
				moovies_list = br.get_moovies(page)
				for moovie in moovies_list:
					moovie_hash = md5(moovie)
					if not check_moovie(moovie_hash) and moovie_hash not in already_choosen and moovie_hash not in const.CURRENT_MOOVIES:
						moovie_info = br.info_moovie(moovie)
						add_moovie_to_schedule(day, res["at"], moovie_hash, moovie_info)
						print("aggiunto: ", moovie_hash)
						already_choosen.append(moovie_hash)
						break
				else:
					page+=1 # if not break

				break #if break. break also the while

def gen_single(day, at):
	br = Browser()
	page = 0
	while 1:
		moovies_list = br.get_moovies(page)
		for moovie in moovies_list:
			moovie_hash = md5(moovie)
			if not check_moovie(moovie_hash) and moovie_hash not in const.CURRENT_MOOVIES and moovie_hash not in const.NOT_TODAY:
				moovie_info = br.info_moovie(moovie)
				add_moovie_to_schedule(day, at, moovie_hash, moovie_info)
				print("aggiunto: ", moovie_hash)
				return 
				#aggiungi
		else:
			page+=1 # if not break

		break #if break. break also the while