import bitly_api, time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

API_TOKEN = '821666523:AAE7FOB90ue50pKL_otDNlamGeQ44wFhY70'

URL_CHECK = "https://altadefinizione01-nuovo.link"
OWN_DOMAIN = "http://212.24.97.183:5000/moovie/"

token_adfocus = "81455d9bfc34bc40210c31e478485657"
token_bitly = "bc2effc32d0e85174c887e287db065cfb346d9ba"

def init():
	global CURRENT_MOOVIES, CURRENT_TIMETABLE, NOT_TODAY, send_post, engine, bitly_conn, driver

	options = Options()
	options.headless = True
	driver = webdriver.Firefox(options=options, executable_path="driver/geckodriver")
	driver.get("https://altadefinizione01-nuovo.link/")
	time.sleep(6)

	bitly_conn = bitly_api.Connection(access_token=token_bitly)

	CURRENT_MOOVIES = {} # Current moovies in ram to be scheduled, check fullfill for format
	CURRENT_TIMETABLE = {} # Current timetale 
	NOT_TODAY = [] # hash of moovies not liked today by admin. #Change_moovie --> should be cleared with scheduler every day. At startup add scehduler event.


scraper = None
filterdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]


days_wrapper = {
	"d1": "Monday",
	"d2": "Tuesday",
	"d3": "Wednesday",
	"d4": "Thursday",
	"d5": "Friday",
	"d6": "Saturday",
	"d7": "Sunday"
}

hours_wrapper = {
	"h1": "8:00",
	"h2": "10:00",
	"h3": "12:00",
	"h4": "14:00",
	"h5": "16:00",
	"h6": "18:00",
	"h7": "20:00",
	"h8": "22:00"
}

CHANNEL = "-1001388171877"
