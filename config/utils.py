import hashlib
from config import const
from config.const import (days_wrapper, hours_wrapper)


def clean_dict(dic):
	for k,v in dic.items():
		clean_value = ""
		flag=""
		if v:
			if "sym" in v:
				flag=v["sym"]
				v = v["value"]

			if v.startswith(" ") and v.endswith(" "):
				clean_value = v[1:-1]
			elif v.startswith(", "):
				clean_value = v[2:-3]
			else:
				clean_value = v

		if flag:
			clean_value = {"value":clean_value, "sym":flag}
		
		if clean_value:
			dic.update({k:clean_value})
		
	return dic

def md5(s):
	return hashlib.md5(s.encode()).hexdigest()

def get_day(tg_code):
	return days_wrapper[tg_code]
def get_hour(tg_code):
	return hours_wrapper[tg_code]

def update_dic(timetable, action):
	if action == "days":
		days_copy = {}
		days = timetable["days"]
		for k, v in days_wrapper.items():
			for day in days:
				if day == v:
					v = v + " *"
			days_copy.update({k:v})

		return days_copy

	elif action == "hours":
		hours_copy = {}
		ats = timetable["at"]
		for k, v in hours_wrapper.items():
			if ats == v:
				v = v + " *"
			hours_copy.update({k:v})

		return hours_copy


def gen_schedule_moovie(moovie_hash, moovie_info, day, ora, obj):

	const.CURRENT_MOOVIES.update({
		moovie_hash:{
			"obj": obj,
			"moovie_info" : moovie_info,
			"day" : day,
			"at":ora
		}
	})

	return const.CURRENT_MOOVIES





