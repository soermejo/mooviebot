from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
from bs4 import BeautifulSoup

from config import const

def get_current_link(url):
	const.driver.get("https://altadefinizione01-nuovo.link/")
	html = const.driver.page_source
	soup = BeautifulSoup(html, "html.parser")
	new_link = soup.find("h2", {"class":"elementor-heading-title elementor-size-default"}).a["href"]
	return new_link

