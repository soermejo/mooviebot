import hashlib

def clean_dict(dic):
	for k,v in dic.items():
		clean_value = ""
		if v.startswith(" ") and v.endswith(" "):
			clean_value = v[1:-1]
		elif v.startswith(", "):
			clean_value = v[2:-3]
		
		if clean_value:
			dic.update({k:clean_value})
		
	return dic

def md5(s):
	return hashlib.md5(s.encode()).hexdigest()



