import mechanicalsoup, re
from core.scrap.csrf.bypass import get_current_link
from config.utils import clean_dict, md5
from config.const import (URL_CHECK, OWN_DOMAIN, OWN_DOMAIN)

class Browser:

	def __init__(self):
		self.CURRENT_URL: str 
		self.browser = mechanicalsoup.StatefulBrowser()
		self.update_url()
		
		
	"""
	void return @None bypass csrf and update url
	"""
	def update_url(self):
		print("Bypassing csrf cloudflare...")
		self.CURRENT_URL = get_current_link(URL_CHECK)
		print(f"Url updated: {self.CURRENT_URL}")


	
	"""
	return @list of fresh links
	"""

	def get_moovies(self, page=0):
		url_links = []
		base_url = f"{self.CURRENT_URL}page/{page}/" 
		self.browser.open(base_url)
		obj = self.browser.get_current_page().find_all("div", {"class":"cover boxcaption"})
		for url in obj:
			url_links.append(url.a["href"].split("/")[3])
		return url_links

	"""
	return @dict of info about a moovie
	"""
	def info_moovie(self, base_url_moovie):

		self.browser.open(self.CURRENT_URL+base_url_moovie)
		html = self.browser.get_current_page()

		moovie_info = {
			"name":None,
			"rating":None, 
			"quality":None,
			"film_length":None,
			"year":None,
			"genre":None,
			"img":None
		}


		#supervideo_raw_url = html.find("a", {"class":"dwnDesk linkDown"})["href"]
		#rating = {"value":html.find("span", {"class":"dato"}).b.string, "sym":"📈 <b>Rating:</b> "}
		name = {"value":html.find("div", {"class":"data"}).h1.string + "\n", "sym":"🗃 "}
		image = html.find("div", {"class":"imagen"}).div.img["src"]
		#trailer = html.find("div", {"class":"trailerplay"}).a["href"]
		
		obj = html.find_all("p", {"class":"meta_dd"}, limit=4)
		for x in obj:

			if x.b["title"] == "Durata":
				moovie_info["film_length"] = {"value":x.contents[-1], "sym":"⏱ <b>Durata |</b> "}
			elif x.b["title"] == "Anno":
				moovie_info["year"] = {"value":x.contents[-1], "sym":"🎞 <b>Anno |</b> "}
			if x.b["title"] == "Qualita":
				moovie_info["quality"] = {"value":x.contents[-1], "sym":"📡 <b>Qualità |</b> "}
			
			"""elif x.b["title"] == "Genere":
				moovie_info["genre"] = ""
				for x in x.contents[3:]:
					if x != " / ":
						moovie_info["genre"] = moovie_info["genre"] + ", " + x.string
				moovie_info["genre"] = {"value":moovie_info["genre"], "sym":"💳 <b>Genere:</b> "}
			"""

		#moovie_info["rating"] = rating
		moovie_info["base_url"] = base_url_moovie
		moovie_info["own_url"] = OWN_DOMAIN + md5(base_url_moovie)
		moovie_info["name"] = name
		moovie_info["img"] = image
		#moovie_info["trailer"] = trailer


		return clean_dict(moovie_info)


	"""
	return @link supervideo from raw
	"""

	def supervideo(self, base_url_moovie):
		self.browser.open(self.CURRENT_URL+base_url_moovie)

		html = self.browser.get_current_page()
		raw_supervideo = html.find("a", {"class":"dwnDesk linkDown"})["href"]

		self.browser.open(self.CURRENT_URL+raw_supervideo)
		supervideo_url = re.findall("https://supervideo.tv\/\w*(?=')", str(self.browser.get_current_page()))[0]
		return supervideo_url


