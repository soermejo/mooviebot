from flask import Flask, escape, request, redirect
from db import base as db
from core.scrap.browser import Browser
from config import const

app = Flask(__name__)

@app.route('/')
def hello():
    name = request.args.get("name", "World")
    return f'Hello, {escape(name)}!'

@app.route('/moovie/<moovie_id>')
def moovie(moovie_id):
	res = db.check_moovie(moovie_id)
	if res:
		br = Browser()
		url = br.supervideo(res[0]["url"])
		return redirect(url)
	else:
		return "Not recognized!"
