from tinydb import TinyDB, Query
from config.utils import md5
from config.const import days_wrapper
from threading import Lock

db = TinyDB('db/store.json')
Moovie = Query()
lock = Lock()

if not db.search(Moovie.timetable.exists()):
	print ( "Creating timetable..." )
	json = {"timetable":{"days":["Friday", "Monday"], "at":"12:00"}}
	db.insert(json)

def add_moovie(base_url):
	lock.acquire()
	md5_moovie = md5(base_url)
	res = False
	if not db.search(Moovie.md5 == md5):
		res = db.insert({"md5":md5_moovie, "url":base_url})
	lock.release()
	return res

def update_hour(hour):
	json = db.get(Moovie.timetable)["timetable"]
	json.update({"at":hour})
	db.update({"timetable":json}, Moovie.timetable)

		

def check_moovie(md5):
	lock.acquire()
	res = db.search(Moovie.md5 == md5)
	lock.release()
	return res

def add_timetable(day=None, at=None, schedule=None):
	lock.acquire()
	json = db.get(Moovie.timetable)["timetable"]
	if day:
		if day not in json["days"]:
			json["days"].append(day)
	if at:
		if at != json["at"]:
			json["at"] = at

	if schedule:
		if schedule not in json["schedule"]:
			json["schedule"].append(schedule)

	res = db.update({"timetable":json}, Moovie.timetable)
	lock.release()
	return res

def remove_from_timetable(day=None, at=None, schedule=None):
	lock.acquire()
	json = db.get(Moovie.timetable)["timetable"]

	if day:
		if day in json["days"]:
			json["days"].remove(day)
	if at:
		if at == json["at"]:
			json["at"] = ""

	if schedule:
		if schedule in json["schedule"]:
			json["schedule"].remove(schedule)

	res = db.update({"timetable":json}, Moovie.timetable)

	lock.release()
	return res

def get_timetable():
	lock.acquire()
	res = db.get(Moovie.timetable)["timetable"]
	lock.release()
	return res
